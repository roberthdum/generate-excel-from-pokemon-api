﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CrearReporte
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // CreateReport();
            // Crear un array de opciones de menú
            string[] menuOptions = { "Crear el reporte          ", "Salir del sistema         " };

            // Inicializar la opción seleccionada en 0
            int selectedOption = 0;
            bool limpiar = true;
            while (true)
            {
                // Mostrar el menú
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Selecciona un Opción con las flechas   ↑ ↓ [ENTRER] Para Aceptar la seleccion");
                Console.ResetColor();


                for (int i = 0; i < menuOptions.Length; i++)
                {
                    if (i == selectedOption)
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.Green;
                    }
                    Console.WriteLine(menuOptions[i]);
                    Console.ResetColor();
                }

                // Leer la entrada del usuario
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);

                // Evaluar la entrada del usuario y tomar una acción
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        selectedOption--;
                        if (selectedOption < 0)
                        {
                            selectedOption = menuOptions.Length - 1;
                        }
                        break;

                    case ConsoleKey.DownArrow:
                        selectedOption++;
                        if (selectedOption >= menuOptions.Length)
                        {
                            selectedOption = 0;
                        }
                        break;

                    case ConsoleKey.Enter:
                        if (selectedOption == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Creando el reporte...");

                            CreateReportAsync();
                            Console.WriteLine("[ENTER] para volver al Menu");

                            Console.ReadLine();
             
                        }
                        else
                        {
                            Console.WriteLine("Saliendo del sistema...");
                            Environment.Exit(0);
                        }
                        break;

                    default:
                        break;
                }
            }

        }

        private static  void CreateReportAsync()
        {
          

            Console.ForegroundColor = ConsoleColor.Red;


            bool hasInternetConnection = false;
            using (var ping = new System.Net.NetworkInformation.Ping())
            {
                try
                {
                   // cambiar por alguna ip interna de la vpn
                    var reply = ping.Send("8.8.8.8", 1000);
                    hasInternetConnection = reply.Status == System.Net.NetworkInformation.IPStatus.Success;
                }
                catch { }
            }

            if (!hasInternetConnection)
            {
                Console.WriteLine("No hay conexión a la VPN. Verifique su conexión y vuelva a intentarlo.");
                return;
            }
            Console.ResetColor();
            if (hasInternetConnection)
            {
                // Hacer una solicitud HTTP para obtener el JSON de la API
                using (HttpClient client = new HttpClient())
                {
                    string jsonResponse = "";
                    try
                    {
                        string url = "https://pokeapi.co/api/v2/pokemon?limit=20000&offset=0";
                        HttpResponseMessage response = client.GetAsync(url).Result;

                        // Leer la respuesta como una cadena JSON
                        jsonResponse = response.Content.ReadAsStringAsync().Result;
                    }
                    catch (HttpRequestException)
                    {
                        Console.WriteLine("No se pudo conectar a la API. Verifique su conexión a la VPN de Andreani y vuelva a intentarlo. [Enter Para Continuar]");
                        Console.ReadLine();
                    }
                    // Convertir la cadena JSON en un objeto JObject
                    if (jsonResponse != "")
                    {
                        string filePath = $"Reporte-{DateTime.Now.ToString("yyyymmddhhmmss")}.xlsx";
                        string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filePath);
                        ExcelReportGenerator.GenerateReport(jsonResponse, filePath);
                    }
                }
            }
          
        }
    }
}