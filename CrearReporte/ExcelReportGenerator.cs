﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
namespace CrearReporte
{
    public static class ExcelReportGenerator
    {
        public static void GenerateReport(string jsonData, string fileName)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            // Leer el JSON
            JObject jsonObject = JObject.Parse(jsonData);
            JArray data = (JArray)jsonObject["results"];

            // Obtener el primer objeto del JSON para obtener los nombres de las propiedades
            JObject firstItem = (JObject)data.First;

            // Crear un archivo de Excel
            FileInfo excelFile = new FileInfo(fileName);
            using (ExcelPackage excelPackage = new ExcelPackage(excelFile))
            {
                // Crear una hoja de Excel
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Datos");

                // Añadir los encabezados de las columnas
                int col = 1;
                foreach (JProperty property in firstItem.Properties())
                {
                    worksheet.Cells[1, col].Value = property.Name;
                    col++;
                }

                // Añadir los registros
                int row = 2;
                foreach (JObject item in data)
                {
                    col = 1;
                    foreach (JProperty property in item.Properties())
                    {
                        worksheet.Cells[row, col].Value = property.Value.ToString();
                        col++;
                    }
                    row++;
                }

                // Guardar el archivo de Excel
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Reporte {fileName} creado ");
                Console.ResetColor();
                excelPackage.Save();
            }
        }
    }
}
